function burgerMenu() {
	const buttons = document.querySelector(".btn__icon, .nav__btn");
	const menuIcons = document.querySelector(".fork-app__menu");
	buttons.addEventListener("click", () => {
		menuIcons.classList.toggle("burger__active");
	});
	function selectLink() {
		const list = document.querySelectorAll(".fork-app-menu__link");
		const items = document.querySelectorAll(".fork-app-menu__item");
		list.forEach((elem) => {
			elem.addEventListener("click", () => {
				if (!elem.classList.contains("active")) {
					list.forEach((item) => {
						item.classList.remove("active");
					});
					elem.classList.add("active");
				};

			});
		});
		items.forEach((e) => {
			e.addEventListener("click", () => {
				if (!e.classList.contains("actives")) {
					items.forEach((item) => {
						item.classList.remove("actives");
					});
					e.classList.add("actives");
				};

			});
		});
	};
	selectLink();
};
burgerMenu();